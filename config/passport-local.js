// //dependencies
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const passportLocalMongoose = require("passport-local-mongoose");
const session = require("express-session");
// const User = require("./models/userSchema");
const Requester = require("./models/requesterSchema");
passport.use(new LocalStrategy(Requester.authenticate()));

// use static serialize and deserialize of model for passport session support
passport.serializeUser(Requester.serializeUser());
passport.deserializeUser(Requester.deserializeUser());

passport.use(
  new LocalStrategy(function (email, password, done) {
    Requester.findOne({ email: email }, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false);
      }
      if (!user.verifyPassword(password)) {
        return done(null, false);
      }
      return done(null, user);
    });
  })
);

module.exports = passport;
