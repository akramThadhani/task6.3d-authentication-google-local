const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20");
//import the keys stored
const keys = require("./keys");
const googleUser = require("../models/googleUserSchema");

//serializing the user we got from the passport.use(google strategy func)
passport.serializeUser((user, done) => {
  done(null, user.id);
});
//de-serializing the user we got from the google strategy func when
// the browser comes back to the server to check if the user is still authenticated
passport.deserializeUser((id, done) => {
  //we get an id back from the browser to check so we find who this id belongs to
  //as this will take time for the DB to execute we use the THEN method
  //to return to us the User from that Id
  googleUser.findById(id).then((user) => {
    //so it will THEN retrieve the user from this id and we
    //send the user we found to the next function

    done(null, user);
  });
});

//client id
// 803447156855-tjen0j18uvcabblj4aeqoalnhl16ckht.apps.googleusercontent.com
//client secret
// V6qeOQyYFoLY7kymqHGLOOnA

//this google strategy takes in 2 parameters (one object with option and a callback func)
passport.use(
  new GoogleStrategy(
    {
      //options for google strategy
      clientID: keys.google.clientID,
      clientSecret: keys.google.clientSecret,
      //this is the redirect url
      callbackURL: "/google/redirect",
    },
    //passport callbck function which gets executed when the user grants access
    //from the google consent screen (because it redirects to tasks route)
    (accessToken, refreshToken, profile, done) => {
      console.log("call back func fired");
      console.log(profile);
      //check if the user exists in the DB - since it takes time we can use
      // .then() which will take in a call back function to return to us the
      // user it gets from the findOne method
      googleUser.findOne({ google_id: profile.id }).then((existingUser) => {
        console.log("in here");
        //so if existing user is present in our DB - then
        if (existingUser) {
          //user exists
          console.log("user exists: " + existingUser);
          //we call done so that it will call the next function in the process
          // which is the serialize user function - as it now has a user
          done(null, existingUser);
        } else {
          //create new user
          //use the googleUser schema to create a new user and save the profile info we get back
          const googleUserDocument = new googleUser({
            google_id: profile.id,
            username: profile.displayName,
            email: profile.emails[0].value,
          });
          googleUserDocument.save().then((newUser) => {
            console.log("new google user saved " + newUser);
            //we call done so that it will call the next function in the process
            // which is the serialize user function - as it now has a user
            done(null, newUser);
          });
        }
      });
    }
  )
);
module.exports = passport;
