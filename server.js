const express = require("express");
//import mongoose
const mongoose = require("mongoose");
//using a validator library(imported)
const validator = require("validator");
//use the keys fle in config folder
const keys = require("./config/keys");
const passportGoogle = require("./config/passport-google");
const session = require("express-session");
const passportLocal = require("passport");
const cookieSession = require("cookie-session");
//using phone number validator
const validatePhoneNumber = require("validate-phone-number-node-js");
//using bycrypt for hashing password
const bcrypt = require("bcrypt");
const saltRounds = 10;
// bring in the routing module for index page
const routes = require("./routes");
const app = express();

app.use(express.static("public"));
//this will create a cookie which takes options
app.use(
  session({
    cookie: { maxAge: 120000 },
    resave: false,
    saveUninitialized: false,
    secret: "$$secret",
  })
);
//so now we want passport first to initialise and THEN use cookies (to create sessions)
//initialize passport
app.use(passportGoogle.initialize());
//now we tell passport to use the cookie session to control the login stuff
app.use(passportGoogle.session());

//so now we want passportLocal first to initialise and THEN use cookies (to create sessions)
//initialize passportLocal
app.use(passportLocal.initialize());
//now we tell passportLocal to use the cookie session to control the login stuff
app.use(passportLocal.session());

//connect to mongodb server and database (which was created)
//password WLT4Vks2YYG69LQa
const username = keys.mongoDBAtlas.username;
const password = keys.mongoDBAtlas.password;
mongoose.connect(
  "mongodb+srv://${username}:${password}@cluster-313.0qryi.mongodb.net/cloudDB?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
mongoose.set("useCreateIndex", true);

//using path
const path = require("path");
// middleware example - to serve static assets to the browser if needed
app.use(express.static(path.join(__dirname, "./pages")));

app.use("/", routes());

let port = process.env.PORT;
if (port == null || port == "") {
  port = 8000;
}
app.listen(port);
