const express = require("express");
const bodyParser = require("body-parser");
const Task = require("../models/taskSchema.js");
const router = express.Router();
const path = require("path");
const passport = require("passport");

router.use(bodyParser.urlencoded({ extended: true }));

module.exports = () => {
  router
    .route("/tasks")
    .get((req, res) => {
      if (req.isAuthenticated) {
        console.log("user is logged in/authenticated");
        res.sendFile(path.join(__dirname, "../pages/reqtasks.html"));
      } else {
        console.log("user not authenticated");
        res.redirect("/login");
      }
      if (req.headers.cookie) {
        console.log(req.headers.cookie);
      } else {
        console.log("user not logged in");
        res.redirect("/login");
      }
    })
    .post((req, res) => {
      const task = new Task({
        task_name: req.body.name,
      });
      task.save((err) => {
        if (err) {
          res.send(err);
        } else res.send("Successfully added a new task!");
      });
    })
    .delete((req, res) => {
      Task.deleteMany((err) => {
        if (err) {
          res.send(err);
        } else {
          res.send("Successfully deleted all tasks!");
        }
      });
    });

  router
    .route("/tasks/:tname")
    .get((req, res) => {
      Task.findOne({ task_name: req.params.tname }, (err, foundTask) => {
        if (foundTask) res.send(foundTask);
        else res.send("No Matched Task Found!");
      });
    })
    .put((req, res) => {
      Task.update(
        { task_name: req.params.tname },
        { task_name: req.body.name },
        { overwrite: true },
        (err) => {
          if (err) {
            res.send(err);
          } else {
            res.send("Successfully updated!");
          }
        }
      );
    })

    .patch((req, res) => {
      Task.update(
        { task_name: req.params.tname },
        { $set: req.body },
        (err) => {
          if (!err) {
            res.send("Successfully updated! ");
          } else res.send(err);
        }
      );
    });

  //routes for google sign in
  router.get(
    "/google",
    passport.authenticate("google", {
      scope: ["profile", "email"],
    })
  );
  router.get(
    "/google/redirect",
    passport.authenticate("google"),
    (req, res) => {
      res.redirect("/tasks");
    }
  );

  //logout handler
  router.get("/logout", (req, res) => {
    //log out function handled through passport
    console.log("logging out user");
    req.logOut();
    res.redirect("/login");
  });

  return router;
};
