const express = require("express");
// create a router object using express router and replace the app object
const router = express.Router();
const path = require("path");
//importing packages relating to passport and authentication & creating a session
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const passport = require("passport");
const Requester = require("../models/requesterSchema");

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(express.static("public"));

module.exports = () => {
  router.get("/login", (request, response) => {
    console.log("in here login route");
    // commented out because now we use 'render' to load our file from views folder
    response.sendFile(path.join(__dirname, "../pages/reqlogin.html"));
  });

  router.post("/login", async (request, response) => {
    //checkemail if user exists
    let requester = await Requester.findOne(
      { email: request.body.username },
      (err, user) => {
        if (err) {
          console.log(err + "user not found");
          return response.redirect("/login");
        } else {
          console.log(user);
          return user;
        }
      }
    );
    if (!requester) {
      console.log(user);
      return response.status(400).send("Incorrect email or password.");
    } else {
      console.log("User is present");
      // check password
      const validPassword = await bcrypt.compare(
        request.body.password,
        requester.password
      );
      if (!validPassword) {
        return response.status(400).send("Incorrect email or password.");
      } else {
        request.login(requester, function (err) {
          if (err) {
            console.log(err);
          } else {
            console.log("ine here now");
            passport.authenticate("local")(request, response, () => {
              return response.redirect("/tasks");
            });
          }
        });
      }
    }
  });

  return router;
};
