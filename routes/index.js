const express = require("express");
// create a router object using express router and replace the app object
const router = express.Router();
//using path
const path = require("path");
//using https to send request
const https = require("https");
//for the post method imports
//import mongoose
const mongoose = require("mongoose");
//using bycrypt for hashing password
const bcrypt = require("bcrypt");
const saltRounds = 10;
//create a model(collection) to use with the schema
//import custom requester Schema module
const Requester = require("../models/requesterSchema");

//using body parser to get data from DOM elements
const bodyParser = require("body-parser");

//middleware (for the app to use)
router.use(bodyParser.urlencoded({ extended: true }));
// // middleware example - to serve static assets to the browser if needed
router.use(express.static(path.join(__dirname, "./pages")));

//import login routes
const loginRoute = require("./login");
//import tasks.js here + also handles google sign in routes
const tasksRoute = require("./tasks");
//import workers.js
const workersRoute = require("./workers");
//import forgot route
const forgotRoute = require("./forgotPassword");
//import google sign in route
const googleRoute = require("./googleSignIn");

module.exports = () => {
  router.get("/", (request, response) => {
    response.sendFile(path.join(__dirname, "./pages/index.html"));
  });

  // we mount the sub pages routes into this function (loginRoute and Tasks Route)
  router.use("/", loginRoute());
  //middleware to use the task route
  router.use("/", tasksRoute());

  router.use("/", workersRoute());

  router.use("/", forgotRoute());

  router.use("/", googleRoute());

  router.post("/", (req, res) => {
    const countryOfResidence = req.body.country;
    const firstName = req.body.Fname;
    const lastName = req.body.Lname;
    const email = req.body.Email;
    let passwordOne = req.body.password1;
    let passwordTwo = req.body.password2;
    const address = req.body.Address1 + req.body.Address2;
    const city = req.body.City;
    const state = req.body.State;
    const zip = req.body.zip;
    const phone = req.body.phone;

    console.log(
      countryOfResidence,
      firstName,
      lastName,
      email,
      passwordOne,
      passwordTwo,
      address,
      city,
      state,
      zip,
      phone
    );
    //function to hash passwords
    function hashPasswords(password) {
      const salt = bcrypt.genSaltSync(saltRounds);
      const hash = bcrypt.hashSync(password, salt);
      password = hash;
      return password;
    }
    //Hashing password1
    passwordOne = hashPasswords(passwordOne);
    //Hashing password2
    passwordTwo = hashPasswords(passwordTwo);
    //creating an instance of the model. this is a document
    const requesterDocument = new Requester({
      countryOfResidence: countryOfResidence,
      fname: firstName,
      lname: lastName,
      email: email,
      password: passwordOne,
      password2: passwordTwo,
      address: address,
      city: city,
      stateOrProvince: state,
      ZipOrPostal: zip,
      resetPasswordToken: "null",
      resetPasswordExpires: null,
      mobilePhone: phone,
    });
    //save the document
    requesterDocument.save((err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Success, Form submitted!");
        //send email and new user details to mailchimp for automated welcome email in 5 minutes
        sendUserToMailChimp(firstName, lastName, email);
        //send the next page (login page) as the response
        res.redirect("/login");
      }
    });
  });
  return router;
};

function sendUserToMailChimp(fname, lname, email) {
  console.log("OK INSIDE SENDTOCHIMP");
  //get your mailchimp apiKey
  const apiKey = "ad63f1e71b6b7a5fad771164ec42e66f-us17";
  //get the unique id for audience for the list_id (included in the Path paramater (https.req))
  const list_id = "48f5d91c7b";

  // create a JS object which will be passed onto mailchimp api
  //with required information according to mailchimp reference list
  const data = {
    // an array property which holds the information (array required by mailchimp)
    members: [
      {
        //email and status is a string type required property
        email_address: email,
        status: "subscribed",
        merge_fields: {
          FNAME: fname,
          LNAME: lname,
        },
      },
    ],
  };
  //converting the data object into a JSON format to be sent to mailchimp
  jsonData = JSON.stringify(data);
  console.log(jsonData);

  //defining the url to include in the http.request paramater
  const url = "https://us17.api.mailchimp.com/3.0/lists/48f5d91c7b";

  //defining options as an object which will contain our api key
  const options = {
    //auth property used for api key
    auth: "ak:ad63f1e71b6b7a5fad771164ec42e66f-us17",
    //method is post as we are posting data to the mail chimp api
    method: "POST",
  };
  //now to send this info to mailchimp - we use the HTTPS request
  const request = https.request(url, options, (res) => {
    res.on("data", (data) => {
      if (res.statusCode === 200) {
        console.log("successfully sent to Mailchimp");
      } else {
        console.log("Failed");
      }
    });
  });

  //we write the request(sending the jsonData which is data object converted to JSON)
  request.write(jsonData);
  //end the request
  request.end();
}
