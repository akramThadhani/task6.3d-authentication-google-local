const express = require("express");
// create a router object using express router and replace the app object
const router = express.Router();
const path = require("path");
const bcrypt = require("bcrypt");
const validator = require("validator");
const saltRounds = 10;
const nodemailer = require("nodemailer");
//importing packages relating to passport and authentication & creating a session
const bodyParser = require("body-parser");
const Requester = require("../models/requesterSchema");
const keys = require("../config/keys");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(express.static("public"));

module.exports = () => {
  router.get("/forgot", (request, response) => {
    console.log("in here forgot password");
    // commented out because now we use 'render' to load our file from views folder
    response.sendFile(path.join(__dirname, "../pages/forgotPassword.html"));
  });

  router.post("/forgot", async (request, response) => {
    //take the email input
    const email = request.body.email;
    console.log(email);

    //creating a token
    const createToken = () => {
      const randNumber = Math.floor(Math.random() * 100000);
      return randNumber;
    };

    const newToken = createToken();
    // add the new token to the user account in the DB
    const userDoc = await Requester.findOneAndUpdate(
      { email: email },
      {
        resetPasswordToken: newToken.toString(),
        resetPasswordExpires: Date.now() + 360000, //expires in 6 minutes
      },
      (err, data) => {
        if (err) {
          console.log(err);
        } else {
          console.log(data);
        }
      }
    );

    userDoc.save();
    console.log("userDoc: ", userDoc);

    // send the email using nodemailer (creating a transport)
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: keys.authorized.email,
        pass: keys.authorized.pass,
      },
    });
    //send the reset password link to the user email
    const mailOptions = {
      from: "iCrowdTasker App",
      to: email,
      subject: "Reset Password on iCrowdTasker App",
      text: "http://tranquil-earth-18905.herokuapp.com/reset/" + newToken,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
        response.redirect("/login");
      }
    });
  });

  //router get method for the reset password link
  router
    .route("/reset/:token")
    .get(async (request, response) => {
      console.log(request.params.token);
      //check the token of the URL if it matches a user with that same token
      await Requester.findOne(
        {
          resetPasswordToken: request.params.token,
          resetPasswordExpires: { $gt: Date.now() },
        },
        function (err, user) {
          if (!user) {
            return response.redirect("/forgot");
          } else {
            //if the token is found and token is not expired then load the page
            console.log(user);
            response.sendFile(
              path.join(__dirname, "../pages/passwordReset.html")
            );
          }
        }
      );
    })
    .post(async (request, response) => {
      // get the passwords entered and hash them
      let passOne = request.body.newPassword;
      let passTwo = request.body.confirmPassword;

      if (validator.isEmpty(passOne)) {
        console.log("Password is required");
        response.send("Password is required");
      }
      if (validator.equals(passOne, passTwo)) {
        console.log("passwords match");
        // Hashing password1
        const passwordOne = hashPasswords(passOne);
        //Hashing password2
        const passwordTwo = hashPasswords(passTwo);
        const userDoc = await Requester.findOneAndUpdate(
          {
            resetPasswordToken: request.params.token,
            resetPasswordExpires: { $gt: Date.now() },
          },
          { password: passwordOne, password2: passwordTwo },
          (err, data) => {
            if (err) {
              console.log(err);
              response.redirect("/forgot");
            } else {
              console.log("Password updated");
              response.redirect("/login");
            }
          }
        );
      } else {
        console.log("Password Should match confirm password");
        response.send("Password Should match confirm password");
      }
    });

  //function to hash passwords
  function hashPasswords(password) {
    const salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(password, salt);
    password = hash;
    return password;
  }

  //post/patch method for creating the new password

  return router;
};
