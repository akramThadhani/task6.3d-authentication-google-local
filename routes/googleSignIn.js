const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const router = express.Router();

const passport = require("passport");

router.use(bodyParser.urlencoded({ extended: true }));

module.exports = () => {
  //routes for google sign in
  router.get(
    "/google",
    passport.authenticate("google", {
      scope: ["profile"],
    })
  );
  router.get(
    "/google/redirect",
    passport.authenticate("google"),
    (req, res) => {
      res.redirect("/tasks");
    }
  );

  //logout handler
  router.get("/logout", (req, res) => {
    //log out function handled through passport
    console.log("logging out user");
    req.logOut();
    res.redirect("/login");
  });

  return router;
};
