const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const workerModel = require("../models/workersSchema");
const router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));

module.exports = () => {
  router
    .route("/workers")
    .get((req, res) => {
      workerModel.find((err, workerList) => {
        if (err) {
          res.send(err);
        } else {
          res.send(workerList);
        }
      });
    })
    .post((req, res) => {
      const worker = new workerModel({
        worker_id: req.body.id,
        worker_name: req.body.name,
        worker_address: req.body.address,
        worker_mobile: req.body.mobile,
        worker_password: req.body.password,
      });
      worker.save((err) => {
        if (err) {
          res.send(err);
        } else res.send("Successfully added a new worker!");
      });
    })
    .delete((req, res) => {
      workerModel.deleteMany((err) => {
        if (err) {
          res.send(err);
        } else {
          res.send("Successfully deleted all workers!");
        }
      });
    });

  router
    .route("/workers/:wid")
    .get((req, res) => {
      workerModel.findOne({ worker_id: req.params.wid }, (err, foundWorker) => {
        if (foundWorker) res.send(foundWorker);
        else res.send("No Matching worker Found!");
      });
    })
    .delete((req, res) => {
      workerModel.deleteOne(
        { worker_id: req.params.wid },
        (err, deleteWorker) => {
          if (deleteWorker) {
            res.send(deleteWorker);
          } else {
            res.send("no worker to delete");
          }
        }
      );
    })
    .put((req, res) => {
      const update = {
        $set: {
          worker_name: req.body.name,
          worker_address: req.body.address,
          worker_mobile: req.body.mobile,
          worker_password: req.body.password,
        },
      };
      workerModel.update(
        { worker_id: req.params.wid },
        update,
        { overwrite: false },
        (err) => {
          if (err) {
            res.send(err);
          } else {
            res.send("All good");
          }
        }
      );
    })
    .patch((req, res) => {
      workerModel.update(
        { worker_id: req.params.wid },
        { $set: req.body },
        (err) => {
          if (!err) {
            res.send("Successfully updated! ");
          } else res.send(err);
        }
      );
    });

  return router;
};
