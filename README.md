# README #

Summary - this document showcases the following functionality/features using passport.js for authentication (local and using google sign up)

- google sign up using google OAuth (passport.js)
- Local passport authentication
- password reset email link
- forgot password pages (one for reset password and one for email reset link)

The app is deployed to Heroku.

Website: http://tranquil-earth-18905.herokuapp.com/login