const mongoose = require("mongoose");
const workerSchema = new mongoose.Schema({
  worker_id: {
    type: String,
    required: "Please enter your worker id",
  },
  worker_name: {
    type: String,
    required: "Please enter your worker name",
  },
  worker_address: {
    type: String,
    required: "Please enter your worker address",
  },
  worker_mobile: {
    type: String,
    required: "Please enter your worker mobile",
  },
  worker_password: {
    type: String,
    required: "Please enter your worker password",
  },
  creation_date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Worker", workerSchema);
