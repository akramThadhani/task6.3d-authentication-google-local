const mongoose = require("mongoose");
const googleUser = new mongoose.Schema({
  //we store the google id so we can identify if its the same user loggin in again
  google_id: String,
  username: String,
  email: String,
});

module.exports = mongoose.model("googleUser", googleUser);
