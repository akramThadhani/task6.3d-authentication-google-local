const express = require("express");
//import mongoose
const mongoose = require("mongoose");
//using a validator library(imported)
const validator = require("validator");
//using phone number validator
const validatePhoneNumber = require("validate-phone-number-node-js");
//import passport local mongoose library
const passportLocalMongoose = require("passport-local-mongoose");

const app = express();
app.use(express.static("public"));

//creating a schema for the model
const requesterSchema = new mongoose.Schema({
  countryOfResidence: {
    type: String,
    required: true,
    validate(value) {
      if (validator.isEmpty(value)) {
        throw new Error("Please select Country");
      }
    },
  },
  fname: {
    type: String,
    required: true,
    validate(value) {
      if (validator.isEmpty(value)) {
        throw new Error("First name is required");
      }
    },
  },
  lname: {
    type: String,
    required: true,
    validate(value) {
      if (validator.isEmpty(value)) {
        throw new Error("First name is required");
      }
    },
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error("Email not valid");
      }
      if (validator.isEmpty(value)) {
        throw new Error("Enter Email address");
      }
    },
  },
  password: {
    type: String,
    minlength: 8,
    validate(value) {
      if (validator.isEmpty(value)) {
        throw new Error("Enter Password ");
      }
    },
  },
  password2: {
    type: String,
    minlength: 8,
    validate(value) {
      if (validator.isEmpty(value)) {
        throw new Error("Password Confirmation is required");
      }
      if (
        validator.equals(toString(value), toString(requesterSchema.password))
      ) {
        console.log("passwords match");
      } else {
        throw new Error("Password Should match confirm password");
      }
    },
  },
  address: {
    type: String,
    validate(value) {
      if (validator.isEmpty(value)) {
        throw new Error("Enter address");
      }
    },
  },
  city: {
    type: String,
    validate(value) {
      if (validator.isEmpty(value)) {
        throw new Error("Enter City");
      }
    },
  },
  stateOrProvince: {
    type: String,
    validate(value) {
      if (validator.isEmpty(value)) {
        throw new Error("Enter address");
      }
    },
  },

  ZipOrPostal: Number,

  resetPasswordToken: String,

  resetPasswordExpires: {
    type: Date,
  },

  mobilePhone: {
    type: String,
    trim: true,
    validate(value) {
      if (validatePhoneNumber.validate(value)) {
        console.log("Valid phone number");
      } else {
        throw new Error("Enter a phone number with country code (+61)");
      }
    },
  },
});
// we add a plugin for the passport-local-mongoose library to connect with
requesterSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("requester", requesterSchema);
