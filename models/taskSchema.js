const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
  task_name: {
    type: String,
    required: "Please enter your task name",
  },
  creation_date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Task", taskSchema);
